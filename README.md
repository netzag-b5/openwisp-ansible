# OpenWISP configuration for the dorm Bärenstraße 5
This repo installs an OpenWISP server by using ansible for orchestration.


## Steps to build the server
Instructions based on https://github.com/openwisp/ansible-openwisp2#usage-tutorial

0. Provide a server with a [supported operating system](https://galaxy.ansible.com/openwisp/openwisp2) reachable by the DNS name set in [`hosts`](hosts)
1. Install [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-in-a-virtual-environment-with-pip) on your own device, not the server designated to run openWISP: https://github.com/openwisp/ansible-openwisp2#install-ansible
```sh
# Ensure having Jinja2 and git installed
pip install Jinja2>=2.11
sudo apt-get install git
```
2. Install openWISP role on your own device: https://github.com/openwisp/ansible-openwisp2#install-this-role
```sh
ansible-galaxy install openwisp.openwisp2
ansible-galaxy collection install "community.general:>=3.6.0"
ansible-galaxy collection install "ansible.posix"
```
3. "Working Directory" ist der Ordner in dem dieses Readme liegt.
4. **Configure TLS Certificate**:
Change `openwisp2_ssl_cert` and `openwisp2_ssl_key` in [`playbook.yml`](playbook.yml), to the path where the desired certificate and key lie **on the server**: https://github.com/openwisp/ansible-openwisp2#ssl-certificate-gotchas
5. Run the playbook on your own device. This will install openWISP on the designated server: https://github.com/openwisp/ansible-openwisp2#run-the-playbook
```sh
ansible-playbook -i hosts playbook.yml -u <user> -k --become -K
```
- Substitute `<user>` with your **production server**'s username.
- The `-k` argument will need the `sshpass` program.
- You can remove `-k`, `--become` and `-K` if your public SSH key is installed on the server.
- More information about the `ansible-playbook` command can be found here: https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html

6.  When Playbook is done running, you can login to openWISP at https://openwisp2.baer.rwth-aachen.de with username and password set to `admin`
### Further important steps
7. Change the password (and the username if you like) of the superuser as soon as possible.
8. Update the `name` field of the default `Site` object to accurately display site name in email notifications.
9. Edit the information of the default organization.
10. In the default organization you just updated, note down the automatically generated _shared secret_ option, you will need it to use the [auto-registration feature of openwisp-config](https://github.com/openwisp/openwisp-config#automatic-registration)
11. This Ansible role creates a default template to update `authorized_keys` on networking devices using the default access credentials. The role will either use an existing SSH key pair or create a new one if no SSH key pair exists on the host machine.

## In this repo:
- [`hosts`](hosts): Here we define the server address openWISP is supposed to run on (https://github.com/openwisp/ansible-openwisp2#create-inventory-file).
- [`playbook.yml`](playbook.yml): Here the configuration for openWISP can be found. You can use [a lot of different variables](https://github.com/openwisp/ansible-openwisp2/tree/master#role-variables) to adjust your openWISP installation. Most of the default values should nevertheless work for our dorm configuration.

## Fun with OpenWISP
- There is a REST API for the OpenWISP controller: https://github.com/openwisp/openwisp-controller#rest-api-reference

## Dorm projects related to this project
The Repository [OpenWRTScripts](https://gitlab.com/netzag-b5/OpenWRTScripts) contains our Network Device Side of the [OpenWISP architecture](https://openwisp.io/docs/general/architecture.html). Thus [OpenWISP Config](https://github.com/openwisp/openwisp-config) inclusive [OpenWISP Monitoring](https://github.com/openwisp/openwrt-openwisp-monitoring).